from flask import Flask, jsonify, request

app = Flask(__name__)

from users import users

@app.route('/user/list', methods=['GET'])
def getUserList():
    return jsonify({
        "users":users
    })

@app.route('/user/<int:id>', methods=['GET'])
def getUserById(id):
    getUser = [user for user in users if user['Id'] == id]
    if (len(getUser) > 0):
        return jsonify({
            "message":  "User Found",
            "user":     getUser[0]
        })
    return jsonify({
        "message": "User Not Found"
    })

@app.route('/user/add', methods=['POST'])
def addUser():
    name            = request.json['name']
    email           = request.json['email']
    age             = request.json['age']
    job_title       = request.json['job_title']
    company_name    = request.json['company_name']
    newUser = {
        "Id": len(users)+1,
        "name":         name,
        "email":        email,
        "age":          age,
        "job_title":    job_title,
        "company_name": company_name
    }
    users.append(newUser)
    return jsonify({
        'message':  "User Created",
        "users":    users
    })

@app.route('/user/<int:id>', methods=['PUT'])
def editUser(id):
    getUser = [user for user in users if user['Id']==id]
    if (len(getUser) > 0):
        getUser[0]['name']          = request.json['name']
        getUser[0]['email']         = request.json['email']
        getUser[0]['age']           = request.json['age']
        getUser[0]['job_title']     = request.json['job_title']
        getUser[0]['company_name']  = request.json['company_name']
        return jsonify({
            "message":  "User Updated",
            "user":     getUser[0]
        })
    return jsonify({
        "message": "User Not Foud"
    })

@app.route('/user/<int:id>', methods=['DELETE'])
def deleteUser(id):
    getUser = [user for user in users if user['Id']==id]
    if (len(getUser) > 0):
        users.remove(getUser[0])
        return jsonify({
            "message":  "User Deleted",
            "users":    users
        })
    return jsonify({
        "message": "User Not Found"
    })

if __name__ == '__main__':
    app.run(debug=True, port=3030)
